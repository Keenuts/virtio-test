#include "Driver.hxx"

extern "C"
{
	#include <kdebugprint.h>

	// Global debug printout level and enable\disable flag
	int virtioDebugLevel;
	int bDebugPrint;
	int driverDebugLevel;
	ULONG driverDebugFlags;

#define TEMP_BUFFER_SIZE 256
#define RHEL_DEBUG_PORT     ((PUCHAR)0x3F8)

	static void DebugPrintFuncSerial(const char *format, ...)
	{
		char buf[TEMP_BUFFER_SIZE];
		NTSTATUS status;
		size_t len;
		va_list list;
		va_start(list, format);
		status = RtlStringCbVPrintfA(buf, sizeof(buf), format, list);
		if (status == STATUS_SUCCESS)
		{
			len = strlen(buf);
		}
		else
		{
			len = 2;
			buf[0] = 'O';
			buf[1] = '\n';
		}
		if (len)
		{
			WRITE_PORT_BUFFER_UCHAR(RHEL_DEBUG_PORT, (PUCHAR)buf, (ULONG)len);
			WRITE_PORT_UCHAR(RHEL_DEBUG_PORT, '\r');
		}
	}

	void InitializeDebugPrints(IN PDRIVER_OBJECT  DriverObject, PUNICODE_STRING RegistryPath)
	{
		VirtioDebugPrintProc = DebugPrintFuncSerial;
		driverDebugFlags = 0xffffffff;

		UNREFERENCED_PARAMETER(DriverObject);
		UNREFERENCED_PARAMETER(RegistryPath);

		bDebugPrint = 1;
		driverDebugLevel = 4;
		virtioDebugLevel = -1;
	}

	tDebugPrintFunc VirtioDebugPrintProc;

}
