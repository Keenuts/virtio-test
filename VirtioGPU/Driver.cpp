#include "Driver.hxx"
#include "virtio_gpu.hxx"
// #pragma code_seg(push)
// #pragma code_seg("INIT")


extern "C"
NTSTATUS
DriverEntry(
	_In_  DRIVER_OBJECT*  pDriverObject,
	_In_  UNICODE_STRING* pRegistryPath)
{
	//Init Debug
	/*
	DbgPrint("Tptp");
	InitializeDebugPrints(_In_ pDriverObject, pRegistryPath);
	*/

	//OutputDebugString("Toto");
	// PAGED_CODE();

	// Initialize DDI function pointers and dxgkrnl
	KMDDOD_INITIALIZATION_DATA InitialData = { 0 };

	InitialData.Version = DXGKDDI_INTERFACE_VERSION;

	InitialData.DxgkDdiAddDevice				    = DdiAddDevice;
	InitialData.DxgkDdiStartDevice					= DdiStartDevice;
	InitialData.DxgkDdiStopDevice					= DdiStopDevice;
	InitialData.DxgkDdiResetDevice					= DdiResetDevice;
	InitialData.DxgkDdiRemoveDevice				    = DdiRemoveDevice;
	InitialData.DxgkDdiDispatchIoRequest			= DdiDispatchIoRequest;
	InitialData.DxgkDdiInterruptRoutine				= DdiInterruptRoutine;
	InitialData.DxgkDdiDpcRoutine					= DdiDpcRoutine;
	InitialData.DxgkDdiQueryChildRelations			= DdiQueryChildRelations;
	InitialData.DxgkDdiQueryChildStatus				= DdiQueryChildStatus;
	InitialData.DxgkDdiQueryDeviceDescriptor		= DdiQueryDeviceDescriptor;
	InitialData.DxgkDdiSetPowerState				= DdiSetPowerState;
	InitialData.DxgkDdiUnload						= DdiUnload;
	InitialData.DxgkDdiQueryAdapterInfo				= DdiQueryAdapterInfo;
	InitialData.DxgkDdiSetPointerPosition		    = DdiSetPointerPosition;
	InitialData.DxgkDdiSetPointerShape				= DdiSetPointerShape;
	InitialData.DxgkDdiIsSupportedVidPn				= DdiIsSupportedVidPn;
	InitialData.DxgkDdiRecommendFunctionalVidPn		= DdiRecommendFunctionalVidPn;
	InitialData.DxgkDdiEnumVidPnCofuncModality		= DdiEnumVidPnCofuncModality;
	InitialData.DxgkDdiSetVidPnSourceVisibility		= DdiSetVidPnSourceVisibility;
	InitialData.DxgkDdiCommitVidPn					= DdiCommitVidPn;
	InitialData.DxgkDdiUpdateActiveVidPnPresentPath = DdiUpdateActiveVidPnPresentPath;
	InitialData.DxgkDdiRecommendMonitorModes		= DdiRecommendMonitorModes;
	InitialData.DxgkDdiQueryVidPnHWCapability		= DdiQueryVidPnHWCapability;
	InitialData.DxgkDdiPresentDisplayOnly			= DdiPresentDisplayOnly;
	InitialData.DxgkDdiSystemDisplayEnable			= DdiSystemDisplayEnable;
	InitialData.DxgkDdiSystemDisplayWrite			= DdiSystemDisplayWrite;
	InitialData.DxgkDdiStopDeviceAndReleasePostDisplayOwnership = DdiStopDeviceAndReleasePostDisplayOwnership;
	
	NTSTATUS Status = DxgkInitializeDisplayOnlyDriver(pDriverObject, pRegistryPath, &InitialData);
	if (!NT_SUCCESS(Status))
		return Status;

	return Status;
}

/*
#pragma code_seg(pop)

#pragma code_seg(push)
#pragma code_seg("PAGE")
*/

VOID
DdiUnload(VOID)
{
	PAGED_CODE();
}

NTSTATUS
DdiAddDevice(
	_In_ DEVICE_OBJECT* pPhysicalDeviceObject,
	_Outptr_ PVOID*  ppDeviceContext)
{
	PAGED_CODE();

	(void)pPhysicalDeviceObject;
	VirtioGPU *device = new(NonPagedPoolNx) VirtioGPU(pPhysicalDeviceObject);

	if (!device)
		return STATUS_NO_MEMORY;

	*ppDeviceContext = device;
	return STATUS_SUCCESS;
}

NTSTATUS
DdiRemoveDevice(
	_In_  VOID* pDeviceContext)
{
	PAGED_CODE();

	VirtioGPU *device = reinterpret_cast<VirtioGPU*>(pDeviceContext);
	delete device;

	return STATUS_SUCCESS;
}

NTSTATUS
DdiStartDevice(
	_In_  VOID*              pDeviceContext,
	_In_  DXGK_START_INFO*   pDxgkStartInfo,
	_In_  DXGKRNL_INTERFACE* pDxgkInterface,
	_Out_ ULONG*             pNumberOfViews,
	_Out_ ULONG*             pNumberOfChildren)
{
	PAGED_CODE();


	VirtioGPU *device = reinterpret_cast<VirtioGPU*>(pDeviceContext);
	return device->StartDevice(pDxgkStartInfo, pDxgkInterface, pNumberOfViews, pNumberOfChildren);
}

NTSTATUS
DdiStopDevice(
	_In_  VOID* pDeviceContext)
{
	PAGED_CODE();

	VirtioGPU *device = reinterpret_cast<VirtioGPU*>(pDeviceContext);
	return device->StopDevice();
}


NTSTATUS
DdiDispatchIoRequest(
	_In_  VOID*                 pDeviceContext,
	_In_  ULONG                 VidPnSourceId,
	_In_  VIDEO_REQUEST_PACKET* pVideoRequestPacket)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)VidPnSourceId;
	(void)pVideoRequestPacket;
	
	return STATUS_SUCCESS;
}

NTSTATUS
DdiSetPowerState(
	_In_  VOID*              pDeviceContext,
	_In_  ULONG              HardwareUid,
	_In_  DEVICE_POWER_STATE DevicePowerState,
	_In_  POWER_ACTION       ActionType)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)HardwareUid;
	(void)DevicePowerState;
	(void)ActionType;

	return STATUS_SUCCESS;
}

NTSTATUS
DdiQueryChildRelations(
	_In_                             VOID*                  pDeviceContext,
	_Out_writes_bytes_(ChildRelationsSize) DXGK_CHILD_DESCRIPTOR* pChildRelations,
	_In_                             ULONG                  ChildRelationsSize)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)pChildRelations;
	(void)ChildRelationsSize;

	return STATUS_SUCCESS;
}

NTSTATUS
DdiQueryChildStatus(
	_In_    VOID*              pDeviceContext,
	_Inout_ DXGK_CHILD_STATUS* pChildStatus,
	_In_    BOOLEAN            NonDestructiveOnly)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)pChildStatus;
	(void)NonDestructiveOnly;

	return STATUS_SUCCESS;
}

NTSTATUS
DdiQueryDeviceDescriptor(
	_In_  VOID*                     pDeviceContext,
	_In_  ULONG                     ChildUid,
	_Inout_ DXGK_DEVICE_DESCRIPTOR* pDeviceDescriptor)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)ChildUid;
	(void)pDeviceDescriptor;

	return STATUS_SUCCESS;
}


//
// WDDM Display Only Driver DDIs
//

NTSTATUS
APIENTRY
DdiQueryAdapterInfo(
	_In_ CONST HANDLE                    hAdapter,
	_In_ CONST DXGKARG_QUERYADAPTERINFO* pQueryAdapterInfo)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pQueryAdapterInfo;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiSetPointerPosition(
	_In_ CONST HANDLE                      hAdapter,
	_In_ CONST DXGKARG_SETPOINTERPOSITION* pSetPointerPosition)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pSetPointerPosition;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiSetPointerShape(
	_In_ CONST HANDLE                   hAdapter,
	_In_ CONST DXGKARG_SETPOINTERSHAPE* pSetPointerShape)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pSetPointerShape;

	return STATUS_SUCCESS;
}


NTSTATUS
APIENTRY
DdiPresentDisplayOnly(
	_In_ CONST HANDLE                       hAdapter,
	_In_ CONST DXGKARG_PRESENT_DISPLAYONLY* pPresentDisplayOnly)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pPresentDisplayOnly;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiStopDeviceAndReleasePostDisplayOwnership(
	_In_  VOID*                          pDeviceContext,
	_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_Out_ DXGK_DISPLAY_INFORMATION*      DisplayInfo)
{
	PAGED_CODE();

	(void)pDeviceContext;
	(void)TargetId;
	(void)DisplayInfo;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiIsSupportedVidPn(
	_In_ CONST HANDLE                 hAdapter,
	_Inout_ DXGKARG_ISSUPPORTEDVIDPN* pIsSupportedVidPn)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pIsSupportedVidPn;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiRecommendFunctionalVidPn(
	_In_ CONST HANDLE                                  hAdapter,
	_In_ CONST DXGKARG_RECOMMENDFUNCTIONALVIDPN* CONST pRecommendFunctionalVidPn)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pRecommendFunctionalVidPn;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiRecommendVidPnTopology(
	_In_ CONST HANDLE                                 hAdapter,
	_In_ CONST DXGKARG_RECOMMENDVIDPNTOPOLOGY* CONST  pRecommendVidPnTopology)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pRecommendVidPnTopology;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiRecommendMonitorModes(
	_In_ CONST HANDLE                                hAdapter,
	_In_ CONST DXGKARG_RECOMMENDMONITORMODES* CONST  pRecommendMonitorModes)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pRecommendMonitorModes;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiEnumVidPnCofuncModality(
	_In_ CONST HANDLE                                 hAdapter,
	_In_ CONST DXGKARG_ENUMVIDPNCOFUNCMODALITY* CONST pEnumCofuncModality)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pEnumCofuncModality;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiSetVidPnSourceVisibility(
	_In_ CONST HANDLE                            hAdapter,
	_In_ CONST DXGKARG_SETVIDPNSOURCEVISIBILITY* pSetVidPnSourceVisibility)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pSetVidPnSourceVisibility;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiCommitVidPn(
	_In_ CONST HANDLE                     hAdapter,
	_In_ CONST DXGKARG_COMMITVIDPN* CONST pCommitVidPn)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pCommitVidPn;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiUpdateActiveVidPnPresentPath(
	_In_ CONST HANDLE                                      hAdapter,
	_In_ CONST DXGKARG_UPDATEACTIVEVIDPNPRESENTPATH* CONST pUpdateActiveVidPnPresentPath)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pUpdateActiveVidPnPresentPath;

	return STATUS_SUCCESS;
}

NTSTATUS
APIENTRY
DdiQueryVidPnHWCapability(
	_In_ CONST HANDLE                       hAdapter,
	_Inout_ DXGKARG_QUERYVIDPNHWCAPABILITY* pVidPnHWCaps)
{
	PAGED_CODE();

	(void)hAdapter;
	(void)pVidPnHWCaps;

	return STATUS_SUCCESS;
}


/*
//END: Paged Code
#pragma code_seg(pop)

#pragma code_seg(push)
#pragma code_seg()
// BEGIN: Non-Paged Code
*/

VOID
DdiDpcRoutine(
	_In_  VOID* pDeviceContext)
{
	(void)pDeviceContext;
}

BOOLEAN
DdiInterruptRoutine(
	_In_  VOID* pDeviceContext,
	_In_  ULONG MessageNumber)
{
	(void)pDeviceContext;
	(void)MessageNumber;

	return true;
}

VOID
DdiResetDevice(
	_In_  VOID* pDeviceContext)
{
	(void)pDeviceContext;
}

NTSTATUS
APIENTRY
DdiSystemDisplayEnable(
	_In_  VOID* pDeviceContext,
	_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_In_  PDXGKARG_SYSTEM_DISPLAY_ENABLE_FLAGS Flags,
	_Out_ UINT* Width,
	_Out_ UINT* Height,
	_Out_ D3DDDIFORMAT* ColorFormat)
{
	(void)pDeviceContext;
	(void)TargetId;
	(void)Flags;
	(void)Width;
	(void)Height;
	(void)ColorFormat;

	return STATUS_SUCCESS;
}

VOID
APIENTRY
DdiSystemDisplayWrite(
	_In_  VOID* pDeviceContext,
	_In_  VOID* Source,
	_In_  UINT  SourceWidth,
	_In_  UINT  SourceHeight,
	_In_  UINT  SourceStride,
	_In_  UINT  PositionX,
	_In_  UINT  PositionY)
{

	(void)pDeviceContext;
	(void)Source;
	(void)SourceWidth;
	(void)SourceHeight;
	(void)SourceStride;
	(void)PositionX;
	(void)PositionY;
}

// END: Non-Paged Code
// #pragma code_seg(pop)