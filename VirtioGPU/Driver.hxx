#pragma once

extern "C"
{
#define __CPLUSPLUS

	// Standard C-runtime headers
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <initguid.h>

	// NTOS headers
#include <ntddk.h>

#ifndef FAR
#define FAR
#endif

	// Windows headers
#include <windef.h>
#include <winerror.h>

	// Windows GDI headers
#include <wingdi.h>

	// Windows DDI headers
#include <winddi.h>
#include <ntddvdeo.h>

#include <d3dkmddi.h>
#include <d3dkmthk.h>

#include <ntstrsafe.h>
#include <ntintsafe.h>

#include <dispmprt.h>
};

#include <VirtIOWdf.h>


VOID
DdiUnload(VOID);

NTSTATUS
DdiAddDevice(
	_In_ DEVICE_OBJECT* pPhysicalDeviceObject,
	_Outptr_ PVOID*  ppDeviceContext);

NTSTATUS
DdiRemoveDevice(
	_In_  VOID* pDeviceContext);
NTSTATUS
DdiStartDevice(
	_In_  VOID*              pDeviceContext,
	_In_  DXGK_START_INFO*   pDxgkStartInfo,
	_In_  DXGKRNL_INTERFACE* pDxgkInterface,
	_Out_ ULONG*             pNumberOfViews,
	_Out_ ULONG*             pNumberOfChildren);

NTSTATUS
DdiStopDevice(
	_In_  VOID* pDeviceContext);


NTSTATUS
DdiDispatchIoRequest(
	_In_  VOID*                 pDeviceContext,
	_In_  ULONG                 VidPnSourceId,
	_In_  VIDEO_REQUEST_PACKET* pVideoRequestPacket);

NTSTATUS
DdiSetPowerState(
	_In_  VOID*              pDeviceContext,
	_In_  ULONG              HardwareUid,
	_In_  DEVICE_POWER_STATE DevicePowerState,
	_In_  POWER_ACTION       ActionType);

NTSTATUS
DdiQueryChildRelations(
	_In_                             VOID*                  pDeviceContext,
	_Out_writes_bytes_(ChildRelationsSize) DXGK_CHILD_DESCRIPTOR* pChildRelations,
	_In_                             ULONG                  ChildRelationsSize);

NTSTATUS
DdiQueryChildStatus(
	_In_    VOID*              pDeviceContext,
	_Inout_ DXGK_CHILD_STATUS* pChildStatus,
	_In_    BOOLEAN            NonDestructiveOnly);

NTSTATUS
DdiQueryDeviceDescriptor(
	_In_  VOID*                     pDeviceContext,
	_In_  ULONG                     ChildUid,
	_Inout_ DXGK_DEVICE_DESCRIPTOR* pDeviceDescriptor);

NTSTATUS
APIENTRY
DdiQueryAdapterInfo(
	_In_ CONST HANDLE                    hAdapter,
	_In_ CONST DXGKARG_QUERYADAPTERINFO* pQueryAdapterInfo);

NTSTATUS
APIENTRY
DdiSetPointerPosition(
	_In_ CONST HANDLE                      hAdapter,
	_In_ CONST DXGKARG_SETPOINTERPOSITION* pSetPointerPosition);

NTSTATUS
APIENTRY
DdiSetPointerShape(
	_In_ CONST HANDLE                   hAdapter,
	_In_ CONST DXGKARG_SETPOINTERSHAPE* pSetPointerShape);


NTSTATUS
APIENTRY
DdiPresentDisplayOnly(
	_In_ CONST HANDLE                       hAdapter,
	_In_ CONST DXGKARG_PRESENT_DISPLAYONLY* pPresentDisplayOnly);

NTSTATUS
APIENTRY
DdiStopDeviceAndReleasePostDisplayOwnership(
	_In_  VOID*                          pDeviceContext,
	_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_Out_ DXGK_DISPLAY_INFORMATION*      DisplayInfo);

NTSTATUS
APIENTRY
DdiIsSupportedVidPn(
	_In_ CONST HANDLE                 hAdapter,
	_Inout_ DXGKARG_ISSUPPORTEDVIDPN* pIsSupportedVidPn);

NTSTATUS
APIENTRY
DdiRecommendFunctionalVidPn(
	_In_ CONST HANDLE                                  hAdapter,
	_In_ CONST DXGKARG_RECOMMENDFUNCTIONALVIDPN* CONST pRecommendFunctionalVidPn);

NTSTATUS
APIENTRY
DdiRecommendVidPnTopology(
	_In_ CONST HANDLE                                 hAdapter,
	_In_ CONST DXGKARG_RECOMMENDVIDPNTOPOLOGY* CONST  pRecommendVidPnTopology);

NTSTATUS
APIENTRY
DdiRecommendMonitorModes(
	_In_ CONST HANDLE                                hAdapter,
	_In_ CONST DXGKARG_RECOMMENDMONITORMODES* CONST  pRecommendMonitorModes);

NTSTATUS
APIENTRY
DdiEnumVidPnCofuncModality(
	_In_ CONST HANDLE                                 hAdapter,
	_In_ CONST DXGKARG_ENUMVIDPNCOFUNCMODALITY* CONST pEnumCofuncModality);

NTSTATUS
APIENTRY
DdiSetVidPnSourceVisibility(
	_In_ CONST HANDLE                            hAdapter,
	_In_ CONST DXGKARG_SETVIDPNSOURCEVISIBILITY* pSetVidPnSourceVisibility);

NTSTATUS
APIENTRY
DdiCommitVidPn(
	_In_ CONST HANDLE                     hAdapter,
	_In_ CONST DXGKARG_COMMITVIDPN* CONST pCommitVidPn);

	NTSTATUS
	APIENTRY
	DdiUpdateActiveVidPnPresentPath(
		_In_ CONST HANDLE                                      hAdapter,
		_In_ CONST DXGKARG_UPDATEACTIVEVIDPNPRESENTPATH* CONST pUpdateActiveVidPnPresentPath);

NTSTATUS
APIENTRY
DdiQueryVidPnHWCapability(
	_In_ CONST HANDLE                       hAdapter,
	_Inout_ DXGKARG_QUERYVIDPNHWCAPABILITY* pVidPnHWCaps);

VOID
DdiDpcRoutine(
	_In_  VOID* pDeviceContext);

BOOLEAN
DdiInterruptRoutine(
	_In_  VOID* pDeviceContext,
	_In_  ULONG MessageNumber);

VOID
DdiResetDevice(
	_In_  VOID* pDeviceContext);

NTSTATUS
APIENTRY
DdiSystemDisplayEnable(
	_In_  VOID* pDeviceContext,
	_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_In_  PDXGKARG_SYSTEM_DISPLAY_ENABLE_FLAGS Flags,
	_Out_ UINT* Width,
	_Out_ UINT* Height,
	_Out_ D3DDDIFORMAT* ColorFormat);

VOID
APIENTRY
DdiSystemDisplayWrite(
	_In_  VOID* pDeviceContext,
	_In_  VOID* Source,
	_In_  UINT  SourceWidth,
	_In_  UINT  SourceHeight,
	_In_  UINT  SourceStride,
	_In_  UINT  PositionX,
	_In_  UINT  PositionY);


/******************************Module*Header*******************************\
* Module Name: Memory.cxx + following header part
* Basic Display Driver memory allocation, deletion, and tracking
* Copyright (c) 2010 Microsoft Corporation
\**************************************************************************/

// Defaulting the value of PoolType means that any call to new Foo()
// will raise a compiler error for being ambiguous. This is to help keep
// any calls to allocate memory from accidentally NOT going through
// these functions.
_When_((PoolType & NonPagedPoolMustSucceed) != 0,
	__drv_reportError("Must succeed pool allocations are forbidden. "
		"Allocation failures cause a system crash"))
	void* __cdecl operator new(size_t Size, POOL_TYPE PoolType = PagedPool);
_When_((PoolType & NonPagedPoolMustSucceed) != 0,
	__drv_reportError("Must succeed pool allocations are forbidden. "
		"Allocation failures cause a system crash"))
	void* __cdecl operator new[](size_t Size, POOL_TYPE PoolType = PagedPool);
void  __cdecl operator delete(void* pObject);
void  __cdecl operator delete(void* pObject, size_t s);
void  __cdecl operator delete[](void* pObject);

// Pool allocation tag for the Sample Display Driver. All allocations use this tag.
#define BDDTAG 'DDBS'