#pragma once

#include "Driver.hxx"

typedef struct _CURRENT_MODE
{
	// The source mode currently set for HW Framebuffer
	// For sample driver this info filled in StartDevice by the OS and never changed.
	DXGK_DISPLAY_INFORMATION             DispInfo;

	// The rotation of the current mode. Rotation is performed in software during Present call
	D3DKMDT_VIDPN_PRESENT_PATH_ROTATION  Rotation;

	D3DKMDT_VIDPN_PRESENT_PATH_SCALING Scaling;
	// This mode might be different from one which are supported for HW frame buffer
	// Scaling/displasment might be needed (if supported)
	UINT SrcModeWidth;
	UINT SrcModeHeight;

	// Various boolean flags the struct uses
	struct _CURRENT_BDD_MODE_FLAGS
	{
		UINT SourceNotVisible : 1; // 0 if source is visible
		UINT FullscreenPresent : 1; // 0 if should use dirty rects for present
		UINT FrameBufferIsActive : 1; // 0 if not currently active (i.e. target not connected to source)
		UINT DoNotMapOrUnmap : 1; // 1 if the FrameBuffer should not be (un)mapped during normal execution
		UINT IsInternal : 1; // 1 if it was determined (i.e. through ACPI) that an internal panel is being driven
		UINT Unused : 27;
	} Flags;

	// The start and end of physical memory known to be all zeroes. Used to optimize the BlackOutScreen function to not write
	// zeroes to memory already known to be zero. (Physical address is located in DispInfo)
	PHYSICAL_ADDRESS ZeroedOutStart;
	PHYSICAL_ADDRESS ZeroedOutEnd;

	// Linear frame buffer pointer
	// A union with a ULONG64 is used here to ensure this struct looks the same on 32bit and 64bit builds
	// since the size of a VOID* changes depending on the build.
	union
	{
		VOID*                            Ptr;
		ULONG64                          Force8Bytes;
	} FrameBuffer;
} CURRENT_MODE;


typedef struct _DRIVER_FLAGS
{
	bool started;
} DRIVER_FLAGS;

class VirtioGPU
{
	private:
		DRIVER_FLAGS		m_Flags;
		DXGK_DRIVERCAPS		m_DriverCaps;

		DXGK_DEVICE_INFO	m_DeviceInfo;
		DEVICE_OBJECT	   *m_PhysicalDevice;
		DXGK_START_INFO		m_StartInfo;
		DXGKRNL_INTERFACE	m_DxgkInterface;
		CURRENT_MODE	    m_CurrentModes;

	public :
		VirtioGPU(DEVICE_OBJECT* physicalDevice); //TODO
		~VirtioGPU();						       //TODO

		//TODO
		//Init VirtIO queues, set driver ready to do some GPU magic
		//Get the framebuffer from DX
		//CheckHardware PCI method, vendor ID and stuff (Check sample code)
		NTSTATUS StartDevice(DXGK_START_INFO *dxgkStartInfo,
							 _In_ DXGKRNL_INTERFACE *dxgkInterface,
							 _Out_ ULONG* viewNbr,
							 _Out_ ULONG* childNbr);

		//TODO
		//Destroy VirtIO queues
		//Give back the framebuffer
		NTSTATUS StopDevice(VOID);

		//NOT TO IMPLEMENT FOR NOW
		NTSTATUS DispatchIoRequest(_In_  ULONG pnSrcId,
						  _In_  VIDEO_REQUEST_PACKET* requestPacket);

		//TODO
		//Return NTSTATUS
		NTSTATUS SetPowerState(_In_  ULONG              hwUid,
							   _In_  DEVICE_POWER_STATE powerState,
							   _In_  POWER_ACTION       action);

		//TODO
		// Setup each children on VideoOutput type and so.
		// Check miniportDisplay exemple
		NTSTATUS QueryChildRelations(_Out_writes_bytes_(ChildRelationsSize) DXGK_CHILD_DESCRIPTOR* pChildRelations,
									 _In_ ULONG ChildRelationsSize);
		//TODO
		//Same here
		NTSTATUS QueryChildStatus(_Inout_ DXGK_CHILD_STATUS* pChildStatus,
					     _In_ BOOLEAN NonDestructiveOnly);

		//TODO
		//Get a child device descriptor, ex, a display
		NTSTATUS QueryDeviceDescriptor(_In_    ULONG                   ChildUid,
							  _Inout_ DXGK_DEVICE_DESCRIPTOR* pDeviceDescriptor);

		//TODO
		//Gives the configuration of our GPU
		//Gives enabled functions
		//Only needs to do the drivercaps part, not extensions (system reserved)
		NTSTATUS QueryAdapterInfo(_In_ CONST DXGKARG_QUERYADAPTERINFO* pQueryAdapterInfo);

		//TODO
		//You know what to do !
		//Note: Thsi can be called,
		//		even if QueryAdaptaterInfo said there is no HW cusror.
		//		(To set it to hidden)
		NTSTATUS SetPointerPosition(_In_ CONST DXGKARG_SETPOINTERPOSITION* pSetPointerPosition);
		
		//TODO
		//Self explanatory
		NTSTATUS SetPointerShape(_In_ CONST DXGKARG_SETPOINTERSHAPE* pSetPointerShape);

		//TODO
		//This function will send a screen to the device
		//IOW: send a command to the HW
		// This function MUST send it, wait for termination, then return FAILED/SUCESS
		// DO not return PENDING, otherwise, you'll foll the OS, and will turn async
		NTSTATUS PresentDisplayOnly(_In_ CONST DXGKARG_PRESENT_DISPLAYONLY* pPresentDisplayOnly);

		//TODO
		//Wipe the screen
		//set powerstate to none
		//Stop the device, releasing the framebuffer
		NTSTATUS StopDeviceAndReleasePostDisplayOwnership(_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
														  _Out_ DXGK_DISPLAY_INFORMATION*      pDisplayInfo);

		//TODO
		// Gives HW capabilities on VidPn path
		// Rotation, Scalem Clone, ColorConvert, and two others (linked and renote output)
		NTSTATUS QueryVidPnHWCapability(_Inout_ DXGKARG_QUERYVIDPNHWCAPABILITY* pVidPnHWCaps);


		//TODO
		//Enable system display for bugcheck
		//Then SystemDisplayWrite will be called to blit some data
		//This must not be paged, because we don't want it to be swapped
		// (Bugcheck context)
		NTSTATUS SystemDisplayEnable(_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
									 _In_  PDXGKARG_SYSTEM_DISPLAY_ENABLE_FLAGS Flags,
									 _Out_ UINT* pWidth,
								 	 _Out_ UINT* pHeight,
									 _Out_ D3DDDIFORMAT* pColorFormat);


		//TODO
		//This function will blit some buffer on the screen.
		//SW implem is in the driver example
		VOID SystemDisplayWrite(_In_reads_bytes_(SourceHeight * SourceStride) VOID* pSource,
								_In_ UINT SourceWidth,
								_In_ UINT SourceHeight,
								_In_ UINT SourceStride,
								_In_ INT PositionX,
								_In_ INT PositionY);

};

#define VIRTIO_CONFIG_ACKNOWLEDGE 1
#define VIRTIO_CONFIG_DRIVER 1
#define VIRTIO_CONFIG_DRIVER_OK 4
#define VIRTIO_CONFIG_FEATURES_OK 8
#define VIRTIO_CONFIG_NEED_REDSET 0x40
#define VIRTIO_CONFIG_FAILED 0x80

enum PCI_SPACE_TYPE
{
	LOCATED_32 = 0,
	RESERVED = 1,
	LOCATED_64 = 2,
	RESERVED = 3
};

#pragma pack()
struct _PCI_CAPACITY
{
	u32 id : 8;
	u32 next : 8;
	DWORD capacities[1];
};
typedef _PCI_CAPACITY PCI_CAPACITY;

#pragma pack()
struct _PCI_BASE_ADDRESS_REG_MEM
{
	u32 memory_space_indocator : 1;
	enum PCI_SPACE_TYPE type   : 2;
	u32 prefetchable : 1;
	u32 base_address : 28;
};
typedef _PCI_BASE_ADDRESS_REG_MEM PCI_BASE_ADDRESS_REG_MEM;

#pragma pack()
struct _PCI_BASE_ADDRESS_REG_IO
{
	u32 IO_space_indicator : 1;
	u32 reserved : 1;
	u32 base_address : 30;
};
typedef _PCI_BASE_ADDRESS_REG_IO PCI_BASE_ADDRESS_REG_IO;

#pragma pack()
struct _CONFIG_SPACE
{
	u32 deviceFeatureSelect;
	u32 deviceFeature;
	u32 driverFeatureSelect;
	u32 driverFeature;
	u16 msixConfig;
	u16 numQueues;
	u8 deviceStatus;
	u8 configGeneration;
};
typedef struct _CONFIG_SPACE CONFIG_SPACE;

#pragma pack()
struct _QUEUE_INFO_FIELD
{
	u16 queueSelect;
	u16 queueSize;
	u16 queueMSixVector;
	u16 QueueEnable;
	u16 QueueNotifyOff;
	u64 QueueDesc;
	u64 QueueAvail;
	u64 QueueUsed;
};
typedef struct _QUEUE_INFO_FIELD QUEUE_INFO_FIELD;

enum VIRTIO_GPU_CTRL_TYPE
{
	VIRTIO_GPU_CMD_GET_DISPLAY_INFO = 0x0100,
	VIRTIO_GPU_CMD_RESOURCE_CREATE_2D,
	VIRTIO_GPU_CMD_RESOURCE_UNREF,
	VIRTIO_GPU_CMD_SET_SCANOUT,
	VIRTIO_GPU_CMD_RESOURCE_FLUSH,
	VIRTIO_GPU_CMD_TRANSFER_TO_HOST_2D,
	VIRTIO_GPU_CMD_RESOURCE_ATTACH_BACKING,
	VIRTIO_GPU_CMD_TRANSFER_DETACH_BAKING,


	VIRTIO_GPU_CMD_UPDATE_CUSOR = 0x0300,
	VIRTIO_GPU_CMD_MOVE_CURSOR,

	VIRTIO_GPU_CMD_OK_NODATA = 0x1100,
	VIRTIO_GPU_CMD_OK_DISPLAY_INFO,


	VIRTIO_GPU_CMD_ERR_UNSPEC,
	VIRTIO_GPU_CMD_ERR_OUT_OF_MEMORY,
	VIRTIO_GPU_CMD_ERR_INVALID_SCANOUT_ID,
	VIRTIO_GPU_CMD_ERR_INVALID_RESOURCE_ID,
	VIRTIO_GPU_CMD_ERR_INVALID_CONTEXT_ID,
	VIRTIO_GPU_CMD_ERR_INVALID_PARAMETER
};

#define VIRTIO_FLAG_FENCE (1 << 0)

struct _VIRTIO_GPU_CTRL_HEADER
{
	u32 type;
	u32 flag;
	u64 fence_id;
	u32 ctx_id;
	u32 padding;
};
typedef _VIRTIO_GPU_CTRL_HEADER VIRTIO_GPU_CTRL_HEADER;
/*
Queue setup from VirtIO

GetDeviceContext(device)
	This will take a WDFDevice and send a context back

WDF_IO_QUEUE_CONFIG_INIT -> send a pointer, and an enum value
	WdfIoQueueDispatchManual by ex, and this will setup the config variable

	device -> a WDFDevice (Need to be retreived when starting the interface)
	config -> From the config func above
	context -> from the context function above
WdfIoQueueCreate( device, config, context)

*/