
#include "virtio_gpu.hxx"
#include "Driver.hxx"

extern "C"
{
	#include <osdep.h>
	#include <VirtIO.h>
	#include <virtio_pci.h>
	#include <virtio_pci_common.h>
}

VirtioGPU::VirtioGPU(DEVICE_OBJECT* physical_device)
	: m_PhysicalDevice(physical_device)
{
	memset(&m_StartInfo, 0, sizeof(m_StartInfo));
	memset(&m_DxgkInterface, 0, sizeof(m_DxgkInterface));
	memset(&m_Flags, 0, sizeof(m_Flags));
	memset(&m_DeviceInfo, 0, sizeof(m_DeviceInfo));
	memset(&m_DriverCaps, 0, sizeof(m_DriverCaps));

	m_DriverCaps.WDDMVersion = DXGKDDI_WDDMv1_2;
	m_DriverCaps.HighestAcceptableAddress.QuadPart = -1;

	m_DriverCaps.SupportNonVGA = TRUE;
	m_DriverCaps.SupportSmoothRotation = TRUE;
}

VirtioGPU::~VirtioGPU()
{

}

NTSTATUS VirtioGPU::StartDevice(DXGK_START_INFO *dxgkStartInfo,
	_In_ DXGKRNL_INTERFACE *dxgkInterface,
	_Out_ ULONG* viewNbr,
	_Out_ ULONG* childNbr)
{
	memcpy(&m_StartInfo, dxgkStartInfo, sizeof(m_StartInfo));
	memcpy(&m_DxgkInterface, dxgkInterface, sizeof(m_DxgkInterface));
	memset(&m_CurrentModes, 0, sizeof(m_CurrentModes));

	m_CurrentModes.DispInfo.TargetId = D3DDDI_ID_UNINITIALIZED;

	// Get device information from OS.
	NTSTATUS Status = m_DxgkInterface.DxgkCbGetDeviceInformation(m_DxgkInterface.DeviceHandle, &m_DeviceInfo);
	if (!NT_SUCCESS(Status))
		return Status;

	//TODO: Add registration values in the register values
	
	// TODO: Add HW check Status = CheckHardware();
	if (!NT_SUCCESS(Status))
		return Status;

	//Get the Framebuffer
	Status = m_DxgkInterface.DxgkCbAcquirePostDisplayOwnership(m_DxgkInterface.DeviceHandle, &(m_CurrentModes.DispInfo));
	if (!NT_SUCCESS(Status) || m_CurrentModes.DispInfo.Width == 0)
		return STATUS_UNSUCCESSFUL;

	ULONG read = 0;
	PULONG pread = &read;
	PCI_COMMON_HEADER pci_header = { 0 };

	//Getting config space, and buffers addresses
	//PCI type = 0
	NTSTATUS stat = m_DxgkInterface.DxgkCbReadDeviceSpace(m_DxgkInterface.DeviceHandle, DXGK_WHICHSPACE_CONFIG, &pci_header, 0, sizeof(PCI_COMMON_HEADER), pread);

	check_hardware(pci_header);
	DbgBreakPoint();

	read_extended_capacities(dxgkInterface, m_DxgkInterface.DeviceHandle, pci_header.u.type0.CapabilitiesPtr);

	unsigned short value = VIRTIO_CONFIG_FEATURES_OK;
	PVOID data = &value;
	m_DxgkInterface.DxgkCbWriteDeviceSpace(m_DxgkInterface.DeviceHandle, DXGK_WHICHSPACE_CONFIG, data, offsetof(PCI_COMMON_HEADER, Status), sizeof(pci_header.Status), pread);
	

	m_Flags.started = TRUE;
	(void)viewNbr;
	(void)childNbr;

	return STATUS_SUCCESS;
}

#define REDHAT_VENDOR_ID 0x1af4
#define REDHAT_GPU_DEVICE_ID 0x1050
#define GRAPHIC_CONTROLLER 0x10

#define VGA_COMPATIBLE_SUBCLASS 0
#define VGA_COMPATIBLE_PROGIF 0

NTSTATUS check_hardware(PCI_COMMON_HEADER header)
{
	if (header.VendorID != REDHAT_VENDOR_ID || header.DeviceID != REDHAT_GPU_DEVICE_ID) //Chech device identy
		return STATUS_DEVICE_FEATURE_NOT_SUPPORTED;

	if (header.BaseClass != GRAPHIC_CONTROLLER && header.HeaderType != 0) // Chech the device type
		return STATUS_DEVICE_FEATURE_NOT_SUPPORTED;

	if (header.SubClass != VGA_COMPATIBLE_SUBCLASS || header.ProgIf != VGA_COMPATIBLE_PROGIF) //Check device capacities
		return STATUS_DEVICE_FEATURE_NOT_SUPPORTED;
}

NTSTATUS read_extended_capacities(DXGKRNL_INTERFACE *interface, HANDLE deviceHandle, unsigned char offset)
{
	ULONG read;
	PULONG pread = &read;

	PCI_CAPACITY capacity;
	do
	{
		NTSTATUS stat = interface->DxgkCbReadDeviceSpace(deviceHandle, DXGK_WHICHSPACE_CONFIG, &capacity, offset, sizeof(PCI_COMMON_HEADER), pread);
		if (stat != STATUS_SUCCESS)
			return stat;
	} while (capacity.next);

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::StopDevice(VOID)
{
	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::DispatchIoRequest(_In_  ULONG pnSrcId,
	_In_  VIDEO_REQUEST_PACKET* requestPacket)
{
	(void)pnSrcId;
	(void)requestPacket;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::SetPowerState(_In_  ULONG              hwUid,
	_In_  DEVICE_POWER_STATE powerState,
	_In_  POWER_ACTION       action)
{
	(void)hwUid;
	(void)powerState;
	(void)action;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::QueryChildRelations(_Out_writes_bytes_(ChildRelationsSize) DXGK_CHILD_DESCRIPTOR* pChildRelations,
	_In_ ULONG ChildRelationsSize)
{
	(void)pChildRelations;
	(void)ChildRelationsSize;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::QueryChildStatus(_Inout_ DXGK_CHILD_STATUS* pChildStatus,
	_In_ BOOLEAN NonDestructiveOnly)
{
	(void)pChildStatus;
	(void)NonDestructiveOnly;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::QueryDeviceDescriptor(_In_    ULONG                   ChildUid,
	_Inout_ DXGK_DEVICE_DESCRIPTOR* pDeviceDescriptor)
{
	(void)ChildUid;
	(void)pDeviceDescriptor;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::QueryAdapterInfo(_In_ CONST DXGKARG_QUERYADAPTERINFO* pQueryAdapterInfo)
{
	(void)pQueryAdapterInfo;
	if (pQueryAdapterInfo->Type != DXGKQAITYPE_DRIVERCAPS)
		return STATUS_SUCCESS;

	if (pQueryAdapterInfo && sizeof(m_DriverCaps) <= pQueryAdapterInfo->OutputDataSize)
		memcpy(pQueryAdapterInfo->pOutputData, &m_DriverCaps, sizeof(m_DriverCaps));
	else return STATUS_INVALID_BUFFER_SIZE;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::SetPointerPosition(_In_ CONST DXGKARG_SETPOINTERPOSITION* pSetPointerPosition)
{
	(void)pSetPointerPosition;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::SetPointerShape(_In_ CONST DXGKARG_SETPOINTERSHAPE* pSetPointerShape)
{
	(void)pSetPointerShape;

	return STATUS_SUCCESS;
}

UINT BPPFromPixelFormat(D3DDDIFORMAT Format)
{
	switch (Format)
	{
	case D3DDDIFMT_UNKNOWN: return 0;
	case D3DDDIFMT_P8: return 8;
	case D3DDDIFMT_R5G6B5: return 16;
	case D3DDDIFMT_R8G8B8: return 24;
	case D3DDDIFMT_X8R8G8B8: // fall through
	case D3DDDIFMT_A8R8G8B8: return 32;
	default: return 0;
	}
}

NTSTATUS VirtioGPU::PresentDisplayOnly(_In_ CONST DXGKARG_PRESENT_DISPLAYONLY* pPresentDisplayOnly)
{
	//TODO: Add powerstate check and visibility. Don't send in this case.


	if (m_CurrentModes.Flags.FrameBufferIsActive)
	{

		// If actual pixels are coming through, will need to completely zero out physical address next time in BlackOutScreen
		m_CurrentModes.ZeroedOutStart.QuadPart = 0;
		m_CurrentModes.ZeroedOutEnd.QuadPart = 0;
		

		BYTE* pDst = (BYTE*)m_CurrentModes.FrameBuffer.Ptr;
		UINT DstBitPerPixel = BPPFromPixelFormat(m_CurrentModes.DispInfo.ColorFormat);
		
		(void)pDst;
		(void)DstBitPerPixel;
		(void)pPresentDisplayOnly;
		//Call blit
		/*
		return m_HardwareBlt.ExecutePresentDisplayOnly(pDst,
			DstBitPerPixel,
			(BYTE*)pPresentDisplayOnly->pSource,
			pPresentDisplayOnly->BytesPerPixel,
			pPresentDisplayOnly->Pitch,
			pPresentDisplayOnly->NumMoves,
			pPresentDisplayOnly->pMoves,
			pPresentDisplayOnly->NumDirtyRects,
			pPresentDisplayOnly->pDirtyRect,
			RotationNeededByFb);
			*/
	}

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::StopDeviceAndReleasePostDisplayOwnership(_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_Out_ DXGK_DISPLAY_INFORMATION*      pDisplayInfo)
{
	(void)TargetId;
	(void)pDisplayInfo;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::QueryVidPnHWCapability(_Inout_ DXGKARG_QUERYVIDPNHWCAPABILITY* pVidPnHWCaps)
{
	(void)pVidPnHWCaps;

	return STATUS_SUCCESS;
}

NTSTATUS VirtioGPU::SystemDisplayEnable(_In_  D3DDDI_VIDEO_PRESENT_TARGET_ID TargetId,
	_In_  PDXGKARG_SYSTEM_DISPLAY_ENABLE_FLAGS Flags,
	_Out_ UINT* pWidth,
	_Out_ UINT* pHeight,
	_Out_ D3DDDIFORMAT* pColorFormat)
{
	(void)TargetId;
	(void)Flags;
	(void)pWidth;
	(void)pHeight;
	(void)pColorFormat;

	return STATUS_SUCCESS;
}

VOID VirtioGPU::SystemDisplayWrite(_In_reads_bytes_(SourceHeight * SourceStride) VOID* pSource,
	_In_ UINT SourceWidth,
	_In_ UINT SourceHeight,
	_In_ UINT SourceStride,
	_In_ INT PositionX,
	_In_ INT PositionY)
{
	(void)pSource;
	(void)SourceWidth;
	(void)SourceHeight;
	(void)SourceStride;
	(void)PositionX;
	(void)PositionY;
}